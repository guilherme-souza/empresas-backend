INSERT INTO investor (id, name, email, password, city, country) VALUES (1, 'Lucas Rizel', 'lucasrizel@ioasys.com.br', '$2a$10$RUhMExm0NFyuM9AKtS5uXerz4WoBn8vgtTg4Ay4BCpJD01sdDy2PK', 'BH', 'Brasil');
INSERT INTO investor (id, name, email, password, city, country) VALUES (2, 'Teste Apple', 'testeapple@ioasys.com.br', '$2a$10$uk.JVezG706xBymRiN5ke.E.gyYHiu4tcvQcgpyBr1G2dttmg3BQ6', 'BH', 'Brasil');
