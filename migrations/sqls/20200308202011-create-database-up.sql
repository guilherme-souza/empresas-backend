-- -----------------------------------------------------
-- Table "investor"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "investor" CASCADE;
CREATE TABLE  "investor" (
  "id" SERIAL,
  "name" TEXT NOT NULL,
  "email" TEXT NOT NULL,
  "password" TEXT NOT NULL,
  "city" TEXT NOT NULL,
  "country" TEXT NOT NULL,
  "photo" TEXT,
  "first_access" BOOLEAN DEFAULT FALSE,
  "super_angel" BOOLEAN DEFAULT FALSE,
  "created_at" TIMESTAMP NOT NULL DEFAULT NOW(),
  "updated_at" TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY ("id")
);


-- -----------------------------------------------------
-- Table "enterprise_type"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "enterprise_type" CASCADE;
CREATE TABLE  "enterprise_type" (
  "id" SERIAL,
  "name" TEXT NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT NOW(),
  "updated_at" TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY ("id")
);


-- -----------------------------------------------------
-- Table "enterprise"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "enterprise" CASCADE;
CREATE TABLE  "enterprise" (
  "id" SERIAL,
  "enterprise_name" TEXT NOT NULL,
  "email_enterprise" TEXT,
  "facebook" TEXT,
  "twitter" TEXT,
  "linkedin" TEXT,
  "phone" TEXT,
  "photo" TEXT,
  "description" TEXT,
  "city" TEXT NOT NULL,
  "country" TEXT NOT NULL,
  "value" DOUBLE PRECISION,
  "share_price" DOUBLE PRECISION,
  "enterprise_type_id" INT NOT NULL REFERENCES enterprise_type(id),
  "created_at" TIMESTAMP NOT NULL DEFAULT NOW(),
  "updated_at" TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY ("id")
);

-- -----------------------------------------------------
-- Table "investor_on_enterprise"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "investor_on_enterprise" CASCADE;
CREATE TABLE  "investor_on_enterprise" (
  "id" SERIAL,
  "investor_id" INT NOT NULL REFERENCES investor(id),
  "enterprise_id" INT NOT NULL REFERENCES enterprise(id),
  PRIMARY KEY ("id")
);
