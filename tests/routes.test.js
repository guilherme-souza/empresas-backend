const request = require('supertest');
const server = require('../src/server');

let app;

beforeEach((done) => {
    app = server.listen(5001, (err) => {
        if (err) return done(err);
        done();
    });
});

describe('Sample test endpoints', () => {
    it('should return 404 on root endpoint', async (done) => {
        request(app)
            .get('/')
            .expect(404, done);
    })
});

afterEach((done) => {
    return app && app.close(done);
});
