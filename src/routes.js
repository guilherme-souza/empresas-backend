const express = require('express');

// available API versions
const routes = express.Router();

const v1 = require('./v1/api');
routes.use('/v1', v1);

module.exports = routes;
