const dao = require('../database/dao');

class BasicDAO {

    constructor(table) {
        this.table = table;
    }

    getById(params) {
        return dao.selectOne({table: this.table, ...params});
    };

    getAll(params, query) {
      return dao.selectMany({table: this.table, params, query});
    }
}

module.exports = BasicDAO;
