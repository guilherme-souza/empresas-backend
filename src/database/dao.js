const db = require('./database');

class DAO {

  static async selectOne({table = '', fields = ['*'], params = {}, additionalQuery = ''}) {
    const query = {
      text: `SELECT ${fields.join(', ')} FROM ${table} ${getParams(params)} ${additionalQuery}`,
      values: getValues(params)
    };
    const result =  await db.executeQuery(query);
    return result && result[0];
  }

  static async selectMany({table = '', fields = ['*'], join = "", params = {}, additionalQuery = ''}) {
    const query = {
      text: `SELECT ${fields.join(', ')} FROM ${table} ${join} ${getParams(params)} ${additionalQuery}`,
      values: getValues(params)
    };
    return await db.executeQuery(query);
  }

  static async insert({table = '', values = {}}) {
    const query = {
      text: `INSERT INTO ${table} ${join(getFields(values))} VALUES ${getInsertValues(values)} RETURNING id`,
      values: Object.values(values)
    };
    const result =  await db.executeQuery(query);
    return result && result[0];
  }
}

/*FUNCTIONS*/

function getValues(params) {
  let values = Object.values(params);
  for (let i = 0; i < values.length; i++) {
    if (isNaN(values[i])) {
      values[i] = `%${values[i]}%`;
    }
  }
  return values;
}

function getFields(params) {
  return Object.getOwnPropertyNames(params);
}

function join(array) {
  return "( ".concat(array.join(', '), " )");
}

function paramIndex(index) {
  return `$${index + 1}`;
}

function getParams(params, baseIndex = 0) {
  let query = 'WHERE 1 = 1';
  const columns = getFields(params);
  columns.forEach((column, index) => {
    const idxValue = paramIndex(baseIndex + index);
    const operator = isNaN(params[column]) ? 'ILIKE' : '=';
    query += ` AND ${column} ${operator} ${idxValue}`;
  });
  return query;
}

function getInsertValues(values) {
  const indexValues = [];
  getFields(values).forEach((column, index) => {
    indexValues.push(paramIndex(index));
  });
  return join(indexValues);
}

module.exports = DAO;
