const express = require('express');
const authRouter = require('./auth/authAPI');
const enterpriseRouter = require('./enterprise/enterpriseAPI');
const {checkToken} = require('./auth/security');

const apiRouter = express.Router({mergeParams: true});

apiRouter.use('/users/auth', authRouter);
apiRouter.use('/enterprises', checkToken, enterpriseRouter);

module.exports = apiRouter;
