const Security = require('./security');
const AuthService = require('./authService');


class AuthController {
  static async login(req, res) {
    const authService = new AuthService();
    const credentials = req.body;
    await authService.emailLogin(credentials, res);
  }
}

module.exports = AuthController;
