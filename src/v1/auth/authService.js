const Security = require('./security');
const AuthDAO = require('./authDAO');


function validateLogin(credentials, investor, res) {
    if (!!investor && Security.compareEncryptPassword({
        encryptPassword: investor.password,
        password: credentials.password
    })) {
        res.cookie(Security.jwt_name, Security.generateJWT(investor), {httpOnly: true});
        delete investor.password;
        res.send({investor, success: true});
    } else {
        res.status(401).send({success: false, errors: ['Invalid login credentials. Please try again.']});
    }
}

class AuthService {

    constructor(props) {
        this.authDAO = new AuthDAO();
    }

    async emailLogin(credentials, res) {
        const investor = await this.authDAO.getByEmail(credentials.email);
        validateLogin(credentials, investor, res);
    }
}

module.exports = AuthService;
