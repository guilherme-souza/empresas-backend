const express = require('express');
const AuthController = require('./authController');
const router = express.Router({mergeParams: true});

router.post('/sign_in', (req, res) => {
  AuthController.login(req, res);
});

module.exports = router;
