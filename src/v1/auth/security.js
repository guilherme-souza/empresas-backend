const Config = require('../../utils/config');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

const secretKey = '61851011722CEA986C8EF50E577E652EC4BAFAD6';
const jwt_name = '_sturtup-games-backend_session';

function generateJWT(user, creationTime = null) {
  const expires = Date.now() + (parseInt(Config.APP.JWT_MAX_DAYS_ALIVE) * 24 * 60 * 60 * 1000);
  if (!creationTime) {
    creationTime = expires;
  }
  return jwt.sign(JSON.stringify({user, expires, creationTime}), secretKey);
}

function decodeJWT(token, cb) {
  return jwt.verify(token, secretKey, cb);
}

function encrypt(string) {
  return bcryptjs.hashSync(string, 10);
}

function compareEncryptPassword({encryptPassword, password}) {
  return bcryptjs.compareSync(password, encryptPassword);
}

function sendAuthError(res) {
  return res.json({
    errors: ["You need to sign in or sign up before continuing."]
  });
}

function checkToken(req, res, next) {
  let token = req.headers['x-access-token'] || req.headers['authorization'];
  if (!!token && token.startsWith('Bearer ')) {
    token = token.slice(7, token.length);
  }

  if (token) {
    decodeJWT(token, (err, decoded) => {
      if (err) {
        sendAuthError(res);
      } else {
        next();
      }
    });
  } else {
    sendAuthError(res);
  }
};

module.exports = {
  secretKey,
  jwt_name,
  generateJWT,
  compareEncryptPassword,
  checkToken
};
