const express = require('express');
const EnterpriseController = require('./enterpriseController');
const router = express.Router({mergeParams: true});

router.get('/:id', (req, res) => {
  EnterpriseController.getOne(req, res);
});

router.get('/', (req, res) => {
  EnterpriseController.get(req, res);
});

module.exports = router;
