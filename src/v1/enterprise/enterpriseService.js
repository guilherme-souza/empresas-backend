const EnterpriseDAO = require('./enterpriseDAO');

class EnterpriseService {

  constructor(props) {
    this.enterpriseDAO = new EnterpriseDAO();
  }

  async getOne(id, res) {
    return await this.enterpriseDAO.getById(id);
  }

  async get(params, res) {
    return await this.enterpriseDAO.getAll(params);
  }
}

module.exports = EnterpriseService;
