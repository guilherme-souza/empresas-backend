const EnterpriseService = require('./enterpriseService');


const enterpriseService = new EnterpriseService();

class EnterpriseController {
  static async getOne({params}, res) {
    const enterprise = await enterpriseService.getOne(params.id, res);
    if (!!enterprise) {
      return res.status(200).send({success: true, enterprise});
    }
    return res.status(404).send();
  }

  static async get({query}, res) {
    const params = {
      enterprise_type_id: query.enterprise_types,
      enterprise_name: query.name,
    };
    const enterprises = await enterpriseService.get(params, res);
    return res.status(200).send({enterprises});
  }
}

module.exports = EnterpriseController;
