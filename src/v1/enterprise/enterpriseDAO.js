const dao = require('../../database/dao');
const BasicDAO = require('../../crud/basicDAO');

class EnterpriseDAO extends BasicDAO {

  constructor(props) {
    super('enterprise');
  }

  async getById(id) {
    const params = {id};
    return await dao.selectOne({table: this.table, params});
  };
}

module.exports = EnterpriseDAO;
