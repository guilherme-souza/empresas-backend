require('dotenv').config();

const {
  PORT, HOST, API_TOKEN, JWT_MAX_DAYS_ALIVE,
  DB_USER, DB_HOST, DB_NAME, DB_PASSWORD, DB_PORT, DEFAULT_DB
} = process.env;

const Config = {
  'APP': {
    'PORT': PORT || 5000,
    'HOST': HOST || 'localhost',
    'JWT_MAX_DAYS_ALIVE': JWT_MAX_DAYS_ALIVE || 7,
  },
  'DATABASE': {
    'DEFAULT_DB': DEFAULT_DB || 'postgres',
    'USER': DB_USER || 'postgres',
    'HOST': DB_HOST || 'localhost',
    'NAME': DB_NAME || 'empresas_backend',
    'PASSWORD': DB_PASSWORD || 'postgres',
    'PORT': DB_PORT || 5432,
  },
};

module.exports = Config;
