# README

This application is a technical test for recruitment. It's based on a start-up system, where exists investor, enterprises and so on.
The proposal is keep the funcionalities of this API (https://empresas.ioasys.com.br/).

#### Run project

- Install dependencies

`npm i`

- Run app

`npm start`

- Run tests

`npm test`

#### Database structure

- investor

  Like the normal 'user' in other applications

- enterprise_type

  Categorical information for enterprises, like an occupation area

- enterprise

   Main idea behind the API, keep the really important stuffs to the user / investor

- investor_on_enterprise

   It's not an explicit table on the exercise, but cross the information about investor on enterprises. I believe that have more info like investment, start date of partnership, but for simplicity and kept it that way.

#### Available users for tests

``` json
[ 
    {"email": "lucasrizel@ioasys.com.br", "password": "12345678"},
    {"email": "testeapple@ioasys.com.br", "password": "12341234"} 
]
```

#### Available endpoints

- {{POST}} `/api/v1/users/auth/sign_in`

Body request
 ``` json
      {
        "email" : {{user_email}},
        "password" : {{user_password}}
      }
  ```
